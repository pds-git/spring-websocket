/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.pds.websocket.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Main data been
 *
 * @author honya
 */
@Entity
@Component
public class MessageBundle implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(MessageBundle.class);
    
    public static class TYPE{
        public static final int MESSAGE = 0;
        public static final int LIVE = 1;
        public static final int USER = 2;
        public static final int GET = 3;
        public static final int STATUS = 4;
    }

    @Id
    private long Id;
    
    @NotNull
    private int type;

    @JsonInclude(Include.NON_NULL)
    @Size(min = 0, max = 50)
    private String mess_key;

    @JsonInclude(Include.NON_NULL)
    @Size(min = 0, max = 1000)
    private String mess_value;
    
    @JsonInclude(Include.NON_NULL)
    private String mess_date;

    // Minifi sended JSON
    @JsonProperty("t")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getId() {
        return Id;
    }

    public void setId(long Id) {
        this.Id = Id;
    }

    @JsonProperty("v")
    public String getValue() {
        return mess_value;
    }

    public void setValue(String value) {
        this.mess_value = value;
    }

    @JsonProperty("k")
    public String getKey() {
        return mess_key;
    }

    public void setKey(String key) {
        this.mess_key = key;
    }

    @JsonProperty("d")
    public String getDate() {
        return mess_date;
    }

    public void setDate(String date) {
        this.mess_date = date;
    }

    @Override
    public String toString() {

        ObjectMapper jsonConvert = new ObjectMapper();
        String json = "";
        try {
            json = jsonConvert.writeValueAsString(this);
        } catch (JsonProcessingException ex) {
            LOG.error("Fail convert MessageBundle object to JSON.");
        }

        return json;

    }
}
