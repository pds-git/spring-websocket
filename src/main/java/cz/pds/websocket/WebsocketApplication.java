package cz.pds.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsocketApplication {

    private static final Logger LOG = LoggerFactory.getLogger(WebsocketApplication.class);
    
    public static void main(String[] args) {
        SpringApplication.run(WebsocketApplication.class, args);

        
        
        /*
        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        String s = "";
        LOG.debug("Pass: " + s + " hash: " + passwordEncoder.encode(s));
        */
    }
}
