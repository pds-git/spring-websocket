/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.pds.websocket;

import cz.pds.websocket.services.MessageBundleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * Handler catch web socket actions and call Message bundle service
 *
 * @author Jan.Lhotka
 */
public final class AppWebSocketHandler extends TextWebSocketHandler {

    private static final Logger LOG = LoggerFactory.getLogger(AppWebSocketHandler.class);

    @Autowired
    MessageBundleService service;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        LOG.debug("WebSocket connection established - session: " + session);
        service.setConnectionOpen(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        LOG.debug("WebSocket connection closed - session: " + session + " status: " + status);
        service.setConnectionClose(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage textMessage) {
        LOG.debug("Text message received: " + textMessage.getPayload());
        service.recieveMessage(session, textMessage);
    }

    @Override
    protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
        LOG.debug("Binary message received: " + message.toString());
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) {
        LOG.error("WebSocket connection error - session: " + session + " error: " + exception);
    }
    
    
}
