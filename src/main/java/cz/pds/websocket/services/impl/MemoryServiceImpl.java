/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.pds.websocket.services.impl;

import java.util.HashSet;
import java.util.Set;
import cz.pds.websocket.model.MessageBundle;
import cz.pds.websocket.services.MemoryService;
import cz.pds.websocket.services.PermMemoryService;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

/**
 * Combinated in RAM and DB implementation
 *
 * @author honya
 */
@Service
public final class MemoryServiceImpl implements MemoryService{

    private static final Logger LOG = LoggerFactory.getLogger(MemoryServiceImpl.class);
    
    // Actual online sessions list (in memory)
    private final Set<WebSocketSession> onlineSessions = new HashSet<>();
    
    // Actual users status <User, Status>
    private final Map<String, MessageBundle> userStatus = new HashMap<>();

    // Messages memory (permanent)
    @Autowired
    private PermMemoryService messages;

    @Override
    public void saveSession(WebSocketSession sesion) {
        onlineSessions.add(sesion);
        LOG.debug("Saved session: " + onlineSessions.size());
    }

    @Override
    public void removeSession(WebSocketSession session) {
        onlineSessions.remove(session);
    }

    @Override
    public Set<WebSocketSession> getAllSessions() {
        return new HashSet<>(onlineSessions);
    }

    @Override
    public void saveMessage(MessageBundle message) {
        messages.save(message);
        LOG.debug("Saved message: " + message + " size: " + messages.count());
    }

    @Override
    public MessageBundle getMessage(long id) {
        LOG.debug("Get message id: " + id);
        return messages.findById((long) id).get();
    }

    @Override
    public long getCountOfMessages() {
        LOG.debug("Get number of messages: " + messages.count());
        return messages.count();
    }
    
    @Override
    public MessageBundle getUserStatus(String user) {
        return userStatus.get(user);
    }
    
    @Override
    public void saveUserStatus(String user, MessageBundle status) {
        userStatus.put(user, status);
    }

    @Override
    public Map<String, MessageBundle> getAllStatus() {
        return new HashMap<>(userStatus);
    }
}
