/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.pds.websocket.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import cz.pds.websocket.model.MessageBundle;
import cz.pds.websocket.services.MemoryService;
import static cz.pds.websocket.model.MessageBundle.TYPE.*;
import cz.pds.websocket.services.MessageBundleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.util.HtmlUtils;

/**
 * Main logic between Web socket and memory
 *
 * @author honya
 */
@Service
public final class MessageBundleServiceImpl implements MessageBundleService {

    private static final Logger LOG = LoggerFactory.getLogger(MessageBundleServiceImpl.class);

    @Autowired
    private MemoryService memory;

    @Autowired
    private ObjectMapper jsonConvert;

    @Value("${app.old_messages_count}")
    private int OldMessagesCount;

    @Value("${app.new_messages_count}")
    private int NewMessagesCount;

    public enum USER_STATUS {
        ONLINE, OFFLINE
    }

    /**
     * Add Session to onlinelist, infom all users
     *
     * @param session
     */
    @Override
    public void setConnectionOpen(WebSocketSession session) {
        memory.saveSession(session);
        sendUsername(session);
        setUserStatus(session, USER_STATUS.ONLINE);
        sendInitStatus(session);
    }

    /**
     * Remove session from onlinelist and inform all
     *
     * @param session
     */
    @Override
    public void setConnectionClose(WebSocketSession session) {
        memory.removeSession(session);
        setUserStatus(session, USER_STATUS.OFFLINE);
    }

    @Override
    public void recieveMessage(WebSocketSession session, TextMessage textMessage) {

        try {
            // Convert JSON message text (payload) to message object
            MessageBundle mess = jsonConvert.readValue(textMessage.getPayload(), MessageBundle.class);

            // Escape HTML tags
            mess.setValue(HtmlUtils.htmlEscape(mess.getValue()));

            // Get type of message
            switch (mess.getType()) {
                case MESSAGE:
                    setIdUserDate(session, mess);
                    memory.saveMessage(mess);
                    sendMessageToAll(session, mess);
                    break;
                case LIVE:
                    // I dont send id and date in live mess
                    mess.setKey(session.getPrincipal().getName());
                    sendMessageToAll(session, mess);
                    break;
                case GET:
                    LOG.debug("Receive get: " + mess.getKey());
                    if ("0".equals(mess.getKey())) {
                        // Send oldest messages on scroll
                        sendOldMessages(session, Integer.valueOf(mess.getValue()), OldMessagesCount);
                    } else if ("1".equals(mess.getKey())) {
                        sendNewMessages(session, Integer.valueOf(mess.getValue()));
                    }
                    break;
                default:
                    LOG.error("Unknown type of message: " + mess.getType());
            }

        } catch (IOException ex) {
            LOG.error("Fail parse object from JSON: ", ex);
        }
    }

    private void setUserStatus(WebSocketSession session, USER_STATUS status) {

        String user = session.getPrincipal().getName();

        /*
        Pokud ještě není uživatel v paměti, nebo je status v paměti jiný
        A zároveň pokud se nejedná o výjimku kdy se přechází do stavu offline
        A je nějaká další session s uživatelem.
        -> Ukládám status a rozesílám ho.
         */
        if ((memory.getUserStatus(user) == null || USER_STATUS.valueOf(memory.getUserStatus(user).getValue()) != status)
                && !(status == USER_STATUS.OFFLINE
                && memory.getAllSessions().parallelStream().filter(sess -> sess.getPrincipal().getName().equals(user)).count() > 0)) {

            MessageBundle mess = new MessageBundle();
            // Tak ukládám a posílám
            mess.setType(STATUS);
            setIdUserDate(session, mess);
            mess.setValue(status.toString());

            LOG.debug("Save and send status: " + status + " user: " + user);

            // Status of user changed -> Save
            memory.saveUserStatus(user, mess);
            // Send user status change
            sendMessageToAll(session, mess);
        }
    }

    private void sendInitStatus(WebSocketSession session) {

        String user = session.getPrincipal().getName();

        LOG.debug("Send init statuses exscept user: " + user);
        
        memory.getAllStatus().forEach((usr, status) -> {
            if (!usr.equals(user)) {
                sendMessage(session, status);
            }
        });
    }

    private void sendUsername(WebSocketSession session) {
        MessageBundle mess = new MessageBundle();
        mess.setType(USER);
        setIdUserDate(session, mess);
        sendMessage(session, mess);
    }

    /**
     * Send number of messages from newest
     *
     * @param session
     * @param number
     */
    private void sendNewMessages(WebSocketSession session, long id) {

        long newestMessage = memory.getCountOfMessages();

        if (newestMessage - id > NewMessagesCount) {
            id = newestMessage - NewMessagesCount;
        }

        for (long i = id; i < newestMessage; i++) {
            sendMessage(session, memory.getMessage(i));
        }
    }

    /**
     * Send number of oldest messages from id
     *
     * @param session
     * @param number
     */
    private void sendOldMessages(WebSocketSession session, int id, int number) {
        if (id > -1) {
            for (int i = id; i > (id - number); i--) {
                sendMessage(session, memory.getMessage(i));
            }
        }
    }

    private void sendMessageToAll(WebSocketSession session, MessageBundle message) {
        memory.getAllSessions().forEach((t) -> sendMessage(t, message));
    }

    /**
     * Send one message to one Web socket session.
     *
     * @param socket
     * @param message
     */
    private void sendMessage(WebSocketSession socket, MessageBundle message) {

        if (message != null) {
            try {
                socket.sendMessage(new TextMessage(message.toString()));
                LOG.debug("Message sended: " + message.toString());

            } catch (IOException ex) {
                LOG.error("Cant send mesage to socket: " + socket, ex);
            }
        }
    }

    private void setIdUserDate(WebSocketSession session, MessageBundle message) {
        // Set Id
        message.setId(memory.getCountOfMessages());

        // Set username
        message.setKey(session.getPrincipal().getName());

        // Set date of receive
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);
        message.setDate(df.format(new Date()));
    }

    // TODO - informace o přečtení
}
