/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.pds.websocket.services;

import cz.pds.websocket.model.MessageBundle;
import java.util.Map;
import java.util.Set;
import org.springframework.web.socket.WebSocketSession;

/**
 * Main memory facade interface
 *
 * @author Jan.Lhotka
 */
public interface MemoryService {

    public void saveSession(WebSocketSession sesion);

    public void removeSession(WebSocketSession session);

    public Set<WebSocketSession> getAllSessions();

    public void saveMessage(MessageBundle message);

    public MessageBundle getMessage(long id);

    public long getCountOfMessages();

    public MessageBundle getUserStatus(String user);

    public void saveUserStatus(String user, MessageBundle status);

    public Map<String, MessageBundle> getAllStatus();

}
