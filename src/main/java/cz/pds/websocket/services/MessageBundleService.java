package cz.pds.websocket.services;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

/**
 * Main send and receive messages logic
 *
 * @author Jan.Lhotka
 */
public interface MessageBundleService {

    public void setConnectionOpen(WebSocketSession session);

    public void setConnectionClose(WebSocketSession session);

    public void recieveMessage(WebSocketSession session, TextMessage textMessage);

}
