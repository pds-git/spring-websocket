package cz.pds.websocket.services;

import cz.pds.websocket.model.MessageBundle;
import org.springframework.data.repository.CrudRepository;

/**
 * Spring boot CRUD DB interface with autoimplementation
 *
 * @author Jan.Lhotka
 */
public interface PermMemoryService extends CrudRepository<MessageBundle, Long> {

}
