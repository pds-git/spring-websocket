/*
 * Global variables ------------------------------------------------------------
 */
// Enable / disable debug log
var enableLog = true;
// Link to REST api
var linkToWebSocket = "/ws";
// WebSocket
var socket;
// Type of messages
const TYPE = {
    MESSAGE: 0,
    LIVE: 1,
    USER: 2,
    GET: 3,
    STATUS: 4
};
// Type of get message (key)
const KIND = {
    OLD: 0,
    NEW: 1
};

/* 
 * Main App data object
 * App data and every interaction with UI --------------------------------------
 */
var app = {
    // Main messages in chat
    messages: [],
    addMessage: function (messageBundle) {
        // Linkify, short link and to the new window
        messageBundle.v = anchorme(messageBundle.v, {truncate: [40, 20], attributes: [{name: "target", value: "_blank"}]});

        if (messageBundle.id > this.maxId) {
            this.messages.push(messageBundle);
            drawNewMessage(messageBundle, false);
        } else if (messageBundle.id < this.minId) {
            this.messages.unshift(messageBundle);
            drawNewMessage(messageBundle, true);
        }
        this.updateId(messageBundle.id);
        newMessageNotify(messageBundle);
    },
    // Which id is first and last viewed?
    minId: Number.MAX_SAFE_INTEGER,
    maxId: -1,
    updateId: function (id) {
        if (id < this.minId) {
            this.minId = id;
        }
        if (id > this.maxId) {
            this.maxId = id;
        }
        log("Min: " + this.minId + " max: " + this.maxId);
    },
    // Number of unreaded messages
    lastUnreadMessage: -1,
    readAll: function () {
        this.lastUnreadMessage = this.maxId;
    },
    get unreadMessages() {
        return this.maxId - this.lastUnreadMessage;
    },
    // Users and status
    user: "",
    status: "",
    set setUser(user) {
        this.user = user;
        headRight.innerHTML = this.user + "<br>"
                + "<span class='small'>" + this.status + "</span>";
    },
    set setStatus(status) {
        this.status = status;
        headRight.innerHTML = this.user + "<br>"
                + "<span class='small'>" + this.status + "</span>";
    },
    // Users and status
    otherUser: "",
    otherStatus: "",
    dateOtherStatus: new Date(1900),
    set setOtherUser(user) {
        this.otherUser = user;
        headLeft.innerHTML = this.otherUser + "<br>"
                + "<span class='small'>" + this.otherStatus
                + " " + addZero(this.dateOtherStatus.getHours()) + ":" + addZero(this.dateOtherStatus.getMinutes()) + "</span>";
    },
    set setOtherStatus(status) {
        this.otherStatus = status;
        headLeft.innerHTML = this.otherUser + "<br>"
                + "<span class='small'>" + this.otherStatus
                + " " + addZero(this.dateOtherStatus.getHours()) + ":" + addZero(this.dateOtherStatus.getMinutes()) + "</span>";
    },
    parseStatus: function (messageBundle) {

        if (messageBundle.k != this.user) {
            log("Parse status: " + messageBundle.v);
            this.otherUser = messageBundle.k;
            var date = new Date(messageBundle.d);
            this.dateOtherStatus = date;
            this.setOtherStatus = messageBundle.v;
        }
    }
};

// UI components
var chatArea, inArea, outArea, liveOutArea, headLeft, headRight,
        emotButton, emotMenu;
// Title of page
var title = document.title;
// Pop-up notification
var notify;
// Sound
var audio = new Audio("files/r2.ogg");

/*
 * Global functions ------------------------------------------------------------
 */
// Init after load body
function init() {
    // Init UI variables
    chatArea = document.getElementById("chat");     // Complete chat area
    inArea = document.getElementById("input");      // Input form
    outArea = document.getElementById("output");    // Output for complette messages
    liveOutArea = document.getElementById("liveOutput"); // Output for live messages
    headLeft = document.getElementById("headLeft"); // Left status - username and status another user
    headRight = document.getElementById("headRight"); // Right status - mee
    emotButton = document.getElementById("emot");   // Button to open emotMenu
    emotMenu = document.getElementById("emotMenu"); // Menu with emoticons

    // Init event listeners
    inArea.addEventListener("keypress", function (event) {
        // Fix compatible with firefox
        var key = event.which || event.keyCode;

        // Enter neodřádkuje
        if (key === 13) {
            event.preventDefault();
        }

        if (inArea.innerText != "" & inArea.innerText != "\n") {
            // Stisknutý enter a vstupní pole není prázdné - pošli celou zprávu
            if (key === 13) {
                sendMessage(newMessageBundle(TYPE.MESSAGE, "", inArea.innerText));
            }
            // Stisknutý mezerník - pošli průběžnou zprávu (8 je backspace)
            // Nefunguje na mobilu a je to tak správně
            if (key === 32) {
                sendMessage(newMessageBundle(TYPE.LIVE, "", inArea.innerText));
            }
        }

    });

    inArea.addEventListener("paste", function (event) {
        log("Vložení");
        placeCaretAtEnd(inArea);
    });

    chatArea.addEventListener("scroll", function () {
        if (chatArea.scrollTop == 0 && app.minId > -1) {
            log("Top of the scroll");
            getMessages(KIND.OLD, app.minId - 1);
        }
    });

    emotButton.addEventListener("click", function () {
        log("Click on emo: " + emotMenu.style.display);
        if (emotMenu.style.display == "") {
            emotMenu.style.display = "block";
        } else {
            emotMenu.style.display = "";
        }

        // Scroll to end
        chatArea.scrollTo(0, chatArea.scrollHeight);
    });

    emotMenu.addEventListener("click", function (event) {
        if (event.target.nodeName == "SPAN") {
            inArea.innerText += event.target.textContent;
            inArea.focus();
        }
    });

    // Close websocket before close page
    window.onbeforeunload = function (e) {
        if (socket !== null) {
            socket.close();
        }
    };

    // Check abality of websocket and start main web socket loop
    if (CheckWebSocket()) {
        InitWebSocket();
    }
}

// Log text if is debug = true
function log(text) {
    if (enableLog) {
        var d = new Date();
        var t = addZero(d.getHours()) + ":" + addZero(d.getMinutes()) + ":" + addZero(d.getSeconds());
        console.log(t + "\t" + text);
    }
}

// Add zero before numbers < 10
function addZero(number) {
    if (number < 10) {
        return "0" + number;
    } else {
        return number;
    }
}

// Extra count of letter in string - because of emojis
function fancyCount(str) {
    return Array.from(str.split(/[\ufe00-\ufe0f]/).join("")).length;
}

// When open the page -> is readed
window.onfocus = function () {
    log("On focus window");
    //unreadMessages = 0;
    app.readAll();
    document.title = title;
};

// Convert HTML special character to clean string
function htmlToString(text) {
    var a = document.createElement("div");
    a.innerHTML = text;
    return a.innerText;
}

/*
 * Web socket ------------------------------------------------------------------
 */

// Is web browser able to use websocket?
function CheckWebSocket() {
    if ("WebSocket" in window) {
        return true;
    } else {
        alert("Váš prohlížeč nepodporuje Web Socket. Pořiďte si nějaký z tohoto tisíciletí.");
        return false;
    }
}

// Main websocket loop
function InitWebSocket() {

    socket = new WebSocket("wss://" + window.location.host + linkToWebSocket);

    // Event listener when a connection is open
    socket.onopen = function () {
        log("WebSocket connection opened");
        app.setStatus = "Online";
        getMessages(KIND.NEW, app.maxId + 1);
    };

    // Event listener when a message is received from the server
    socket.onmessage = function (message) {
        log("Message received: " + message.data);
        receiveMessage(message.data);
    };

    // Connection close
    socket.onclose = function () {
        log("WebSocket connection closed.");
        socket = null;
        app.setStatus = "Offline";
        window.setTimeout(InitWebSocket, 15000);
    };

    // Connection error
    socket.onerror = function () {
        log("WebSocket connection error.");
        // Volá se i onclose
    };
}

function sendMessage(messageBundle) {
    // Send to opened socket
    if (socket !== null) {

        // Convert javasript object to JSON string
        var jsonText = JSON.stringify(messageBundle);
        socket.send(jsonText);

        if (messageBundle.t == TYPE.MESSAGE) {
            // Clear imput
            inArea.innerText = "";
            inArea.innerHTML = "";
        }

    } else {
        log("Cant send message - socket is close or message is empty.");
    }
}

// Create new Json object from text form
function newMessageBundle(type, key, value) {

    var messageBundle = {
        t: type,
        //id: 0,
        k: key, // Key - username
        v: value   // Value - text of message
                //d: ""
    };

    return messageBundle;
}

function receiveMessage(messageText) {

    // Parse object from JSON
    var messageBundle = JSON.parse(messageText);

    // By type of message
    switch (messageBundle.t) {
        case TYPE.MESSAGE:
            app.addMessage(messageBundle);
            break;
        case TYPE.LIVE:
            drawLiveMessage(messageBundle);
            newLiveMessageNotify();
            break;
        case TYPE.USER:
            app.setUser = messageBundle.k;
            break;
        case TYPE.STATUS:
            app.parseStatus(messageBundle);
            break;
        default:
            log("Unknown type of message: " + messageBundle.t);
    }
}

function getMessages(kind, id) {
    sendMessage(newMessageBundle(TYPE.GET, kind, id));
}

/*
 * UI actions ------------------------------------------------------------------
 */

function drawLiveMessage(messageBundle) {
    // I dont want to see my live chat
    if (messageBundle.k == app.user) {

        drawLiveSended(messageBundle);

    } else {

        liveOutArea.innerHTML = "<div class='message'><span class='msg_info'>LIVE"
                + "</span><br><span class='msg_text'>"
                + messageBundle.v
                + " ...</span></div>";
    }
    chatArea.scrollTo(0, chatArea.scrollHeight);
}

function drawNewMessage(messageBundle, old) {
    log("Draw message: " + app.messages.length + " id: " + messageBundle.id);

    // Get date format from json text format
    var date = new Date(messageBundle.d);
    var cls = "";

    // New Message div element with class message
    var newDiv = document.createElement("div");
    newDiv.className = "message";
    // Set tooltip
    newDiv.title = date.getDate() + ". " + (date.getMonth() + 1) + ". " + date.getFullYear()
            + " user: " + messageBundle.k + " Id: " + messageBundle.id;

    // If it is messages from me - to the right
    // Else set username of other side
    if (messageBundle.k == app.user) {
        newDiv.className += " right";
    } else {
        app.setOtherUser = messageBundle.k;
        liveOutArea.innerHTML = "";
    }

    // Emojis - 2 char normal, but one in UTF16
    if (messageBundle.v.length == 2 && fancyCount(messageBundle.v) == 1) {
        log("Je xxl.");
        cls = " xxl";
    }

    newDiv.innerHTML = "<span class='msg_info'>"
            + addZero(date.getHours())
            + ":" + addZero(date.getMinutes())
            + ":" + addZero(date.getSeconds())
            + "</span><br><span class='msg_text" + cls + "'>"
            + messageBundle.v
            + "</span></div>";

    // It is old (to top) od new (to end) message?
    if (!old) {
        // Add new div to output area
        outArea.appendChild(newDiv);
        chatArea.scrollTo(0, chatArea.scrollHeight);    // Scroll to end
    } else {
        var firstMess = outArea.getElementsByClassName("message")[0];
        outArea.insertBefore(newDiv, firstMess);
        chatArea.scrollTo(0, 20);   // Little scroll down
    }
}

function drawLiveSended(messageBundle) {
    // New live sended
    var aktText = Array.from(inArea.innerText);
    var finalOut = "<span class='z'>";

    var l = messageBundle.v.length;

    // Pro každé písmenko z inputu
    for (i = 0; i < aktText.length; i++) {
        finalOut += aktText[i];
        // Pokud je délka stejná jako status z message bundlu, tak uzavři
        if (i === l) {
            finalOut += "</span>";
        }
    }
    inArea.innerHTML = finalOut;
    placeCaretAtEnd(inArea);
}

// Umístí ukazatel úpravy textu na konec
function placeCaretAtEnd(el) {
    el.focus();
    if (typeof window.getSelection != "undefined"
            && typeof document.createRange != "undefined") {
        var range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
    }
}

// Check focus and notify new message
function newLiveMessageNotify() {
    if (!document.hasFocus() && app.unreadMessages < 1) {
        document.title = "▸ " + title;
    }
}

// Check focus and notify new message if exist
function newMessageNotify(messageBundle) {
    if (!document.hasFocus() && app.unreadMessages > 0) {
        audio.play();
        document.title = "▸ (" + app.unreadMessages + ") " + title;
        newWebNotify(messageBundle);
    } else {
        app.readAll();
    }
}

function newWebNotify(messageBundle) {

    if (window.Notification && Notification.permission !== "denied") {
        Notification.requestPermission(function (status) {  // status is "granted", if accepted by user

            // Close and remove old notify
            if (notify != null) {
                notify.close();
                notify = null;
            }

            notify = new Notification("Chat " + app.unreadMessages + " nepřečtené zprávy", {
                body: messageBundle.k + ": " + htmlToString(messageBundle.v),
                icon: "files/logo.png"
            });
        });
    }
}

// TODO - unlogin
// TODO - Opravit notifikace v chomu na android
// TODO - při změně velikosti seskrolovat dolu

