/* 
 * Copyright (C) 2019 Jan.Lhotka
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Author:  Jan.Lhotka
 * Created: 16.1.2019
 */

-- Make schema for autentification

CREATE TABLE IF NOT EXISTS users (
	username varchar(50) not null primary key,
	password varchar(200) not null,
	enabled boolean not null
);

CREATE TABLE IF NOT EXISTS authorities (
	username varchar(50) not null,
	authority varchar(50) not null,
	constraint fk_authorities_users foreign key(username) references users(username)
);